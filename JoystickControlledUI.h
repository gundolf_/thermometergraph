#include <Arduino.h>
#include "MultiTimer.h"

/**
 * This class defines the basics of the Joystick controlled user interface.
 * Objects of this class should be used in another class or this class should be extended before use.
 */
class JoystickControlledUI : RepeatableMultiTimer {
    // set the number of milliseconds between consecutive joysick polls
    static const uint8_t timeStepMillis = 10;
    // target joystick that will controll this UI
    PotJoystick& joystick;
    // minimum time (multiply by timeStepMillis to get the time in millis) of the joystick to be tilted in one direction, for it to count as a move
    static const uint8_t minTimeForMove = 2;
    // minimum time (multiply by timeStepMillis to get the time in millis) for a joystick to be tilted, before a move is repeated
    static const int repeatSensitivity = 50;
    // counters of x and y axis tilt times (used to detect when moves happen)
    int xMoveTime, yMoveTime;

public:
    // variables to change when a move occurs
    int8_t* xVariable;
    int8_t* yVariable;

public:
    /**
     * The only constructur, takes one paramater, that is the joystick object to be used for the UI
     */
    JoystickControlledUI (PotJoystick& j) : joystick (j) {
        setPeriod(timeStepMillis);
    }

    /**
     * Get the joystick object, which is used in this UI
     * @return the joystick object used in this UI
     */
    PotJoystick& getPotJoystick() {
        return joystick;
    }

    /**
     * Querry if button was released; acknowledge should be sent when this flag is read as set
     * @return 'button was released' flag state
     */
    bool getButonReleasedFlag() {
        return joystick.getButonReleasedFlag();
    }

    /**
     * Acknowledge that the 'button was released' state was read; calling this function causes the flag to be reset
     */
    void acknowledgeButtonReleasedFlag() {
        joystick.clearReleasedFlag();
    }

  private:
    /**
     * Interrupt service routine for timer interrutps
     */
    void onTimer() {
        auto jState = joystick.readState();
        if ((abs (jState.x) > 64) && (xVariable != nullptr)) {
            //
            xMoveTime++;
            if (xMoveTime == repeatSensitivity) {
                xMoveTime = minTimeForMove;
            }
            if (xMoveTime == minTimeForMove) {
                // change the active screen index
                *xVariable += (jState.x > 0 ? 1 : -1);
            }
        } else {
            xMoveTime = 0;
        }
        if ((abs (jState.y) > 64) && (yVariable != nullptr)) {
            yMoveTime++;
            if (yMoveTime == repeatSensitivity) {
                yMoveTime = minTimeForMove;
            }
            if (yMoveTime == minTimeForMove) {
                *yVariable += (jState.y > 0 ? 1 : -1);
            }
        } else {
            yMoveTime = 0;
        }
    }
};
