#ifndef POT_JOYSTICK_H
#define POT_JOYSTICK_H
#include <Arduino.h>

/**
 * Small joystic implemented as two potentiometers and a switch; for example Ky-023
 *
 * Joystick is initialized by calling setupPins() or initializing pins manually, then calling setCentralPosition() when
 * the joystick is guaranteed to be in neutral/central position. Joystick's range of movement will be adjusted on the
 * fly, while the joystick is being moved.
 *
 * Calling getState() wil lreturn the state as position in x ynd y, mapped to the full range of signed byte, and
 * the state of button (HIGH or LOW). Sideeffect of calling this function is the recalculated range of movement.
 *
 * TODO: add setup for buttton active LOW or HIGH
 *
 **/
class PotJoystick {
    // pins
    uint8_t pinX, pinY, pinBt, flags;
    // values for the central position
    int cx, cy = -1;
    int movementRange = 1;

    enum {
        flag_initialized = 1,               // this joystick object is initializes
        flag_button_active_low = 2,         // button on the joystick is active (considered pressed) on LOW state
        flag_button_is_pressed = 4,         // button was pressed the last time state was read
        flag_button_was_released = 8,       // button was released (went from pressed to not pressed) when state was last read
        flag_button_marked_released = 0x10, // button is marked as 'was released' until explicitly acknowledging this state change
        //flag_continuous_range_checking = 4,
        flag_dummy_unused
    };

public:
    struct State {
        int8_t x, y, flags;

        /**
         * get relative (in percents) tilt form central position in x axis (can go into negative too)
         * @return relative tile in x axis
         */
        int8_t getRelativeX() const { return 100*x / 127; }
        /**
         * get relative (in percents) tilt form central position in y axis (can go into negative too)
         * @return relative tile in y axis
         */
        int8_t getRelativeY() const { return 100*y / 127; }

        /**
         * Querry if the button is pressed
         * @return true if button is pressed
         */
        bool isButtonPressed() const {
            return (flags & flag_button_is_pressed) > 0;
        }

        /**
         * Query if the button was released
         * @return true if button just underwent state change from pressed to released
         */
        bool wasButtonreleased() const {
            return (flags & flag_button_was_released) > 0;
        }

        /**
         * Check the 'button released flag', that is a flag which activates on button release and remains active until
         * explicitly reset. Similar to @link #wasButtonreleased(), but more robust - not required to check this flag
         * on every single readState
         * @return true if the button was released and the release flag was not explicitly reset
         */
        bool buttonReleasedFlag() const {
            return (flags & flag_button_marked_released) > 0;
        }

        State (int8_t x, int8_t y, int8_t f) : x(x), y(y), flags(f) {}
    };

public:
    PotJoystick(int pinX, int pinY, int pinBt) : pinX(pinX), pinY(pinY), pinBt(pinBt) {
    }

    /**
     * Initialize pins (this can be done manually if necessary, for example if INPUT_PULLUP is required)
     **/
    void setupPins() {
        pinMode(pinX, INPUT);
        pinMode(pinY, INPUT);
        pinMode(pinBt, INPUT_PULLUP);
    }

    /**
     * To initialize the joystick, put joystick into nevtral (center) position with button released and call this function
     **/
    void initCentralPosition() {
        int vx = analogRead(pinX);
        int vy = analogRead(pinY);
        if (digitalRead(pinBt) == HIGH)
            flags |= flag_button_active_low;

        flags |= flag_initialized;
        // TODO: average the values
        cx = vx;
        cy = vy;
        movementRange = 1;
    }

    /**
     * After the joystick is fully initialized, read its state (position + button state) throug this function
     **/
    State readState() {
        if ((flags & flag_initialized) == 0) {
            // not initialized yet
            return State(0, 0, LOW);
        } else {
            // raw readings
            int vx = analogRead(pinX);
            int vy = analogRead(pinY);
            int bt = digitalRead(pinBt);

            // determine switch transitions
            if (bt == activeState()) {
                // mark the press
                flags |= flag_button_is_pressed;
                flags &= ~flag_button_was_released;
            } else {
                // button is in not-pressed state
                if ((flags & flag_button_is_pressed) > 0) {
                    // button was pressed when state was read previously, this means a state change was just observed; merk this state change
                    flags &= ~flag_button_is_pressed;
                    flags |= flag_button_was_released | flag_button_marked_released;
                } else {
                    // was not pressed before too, so nothing's happened; clear the 'was just released flag', which is only set when state changes
                    flags &= ~flag_button_was_released;
                }
            }

            recalculateRange(vx, vy);

            return State(vx, vy, flags);
        }
    }

    /**
     * Clear the 'button was released' flag; clearing the flag usually means that the button state change has been handled.
     */
    void clearReleasedFlag() {
        flags &= ~flag_button_marked_released;
    }

    /**
     * See if the button is marked as 'was released'; this is a shorcut to readState, if only the button state is to be queried
     * @return true if button was flagged 'was released', false otherwise
     */
    bool getButonReleasedFlag() {
        return (flags & flag_button_marked_released) > 0;
    }

    /**
     * Check if this joystich has been initialized (tilted around a bit) yet
     * @return true if the joystick has been initialized yet (any of the init functions has been called)
     **/
    bool isInitialized() const {
        return (flags & flag_initialized) > 0;
    }

    /**
     * query joystick object for the movement range of joystick, that is, how far can it tilt in all directions
     * @return the range in ADC quants
     */
    int getMovementRange() const {
        return movementRange;
    }

    /**
     * Perform an initialization usng external source (such as values sored in EEPROM)
     * @param cx  central x position
     * @param cy  central y position
     * @param rng range of movement
     * @param activeLow switch is active in LOW
     */
    void externalInit(int cx, int cy, int rng, bool activeLow=true) {
        this->cx = cx;
        this->cy = cy;
        movementRange = rng;
        // override all flags
        flags = flag_initialized;
        if (activeLow)
            flags |= flag_button_active_low;
    }

    /**
     * Get the initialization values, that is central position for x and y and the movement range.
     * Use his function to store these values to external storage or EEPROM.
     * @param cx  [description]
     * @param cy  [description]
     * @param rng [description]
     */
    void getInitValues(int& cx, int& cy, int& rng) {
        cx = this->cx;
        cy = this->cy;
        rng = movementRange;
    }

    /**
     * Check if thew button is pressed
     **/
    uint8_t getButtonState() {
        return digitalRead(pinBt);
    }

    /**
     * Get the active state for switch (either HIGH or LOW)
     * @return HIGH if active state is high/1 or LOW if active state is low/0
     */
    uint8_t activeState() const {
        return ((flags & flag_button_active_low) > 0) ? LOW : HIGH;
    }

private:

    /**
     * Given the input values vx and vy, recalculate the range of the potentiometers, then map the inputs to range of signed byte
     **/
    void recalculateRange(int& vx, int& vy) {
        // recalculate the range if necessary
        if (abs(vx - cx) > movementRange)
            movementRange = abs(vx - cx);
        if (abs(vy - cy) > movementRange)
            movementRange = abs(vy - cy);
        // translate the (vx, vy) to the full range of int8_t
        vx = map(vx, cx - movementRange, cx + movementRange, -127, 127);
        vy = map(vy, cy - movementRange, cy + movementRange, -127, 127);
    }
};

#endif //POT_JOYSTICK_H
