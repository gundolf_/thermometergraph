#ifndef TEMPERATURE_SENSOR_DHT11
#define TEMPERATURE_SENSOR_DHT11
#include <Arduino.h>

/**
 * Made for the KY-015 DHT11 Temperature and humidity sensor
 * sourced from: https://tkkrlab.nl/wiki/Arduino_KY-015_Temperature_and_humidity_sensor_module
 * note: although the sensor returns data in .1 °C and .1 %RH, all the data seem to be integers
 *
 * Sensor pinout; looking on its perforated side, pins are numbered from left to right:
 *  1  VDD
 *  2  SDA
 *  3  GND
 *  4  SCL (or GND, when used as 1-wire)
 *
 * KY015 Module pinout (left to right)
 *  1  signal
 *  2  VCC
 *  3  GND
 * On this module VCC is connected to VCC and GND, SCL is used as GND, and SDA is used for he 1-wire data
 *
 **/
class TemperatureSensor_DHT11 {
    uint8_t h, hd, t, td, pin;

public:
    TemperatureSensor_DHT11(uint8_t pin) : pin(pin) {
        pinMode(pin, INPUT_PULLUP);
    }

    uint8_t readByte() {
        uint8_t b = 0;
        for (int i = 0; i < 8; i ++) {
            // Reading sensor bit by bit. Each bit is sent as a 50 microsecond low pulse
            // followed by a variable length high pulse. If the high pulse is ~28 us
            // then it's a 0 and if it's ~70 us then it's a 1.

            // wait out the low pulse (should be 50 us max); even if it is not there is not much we can do...
            int t50=timeRamp(pin, HIGH, 160);
            // now time the high pulse
            int tUnknown=timeRamp(pin, LOW, 90);
            if (tUnknown > t50) {
                // pulse was longer than 40, this is 1
                b |= (1 << (7-i));
            }
        }
        return b;
    }

    static bool waitRamp(int pin, uint8_t waitForValue, long timeout) {
        long t = micros();
        while (digitalRead(pin) != waitForValue) {
            long t1 = (micros()-t);
            if (t1 > timeout)
                return false;
        }
        return true;
    }

    static int timeRamp(int pin, uint8_t waitForValue, uint16_t timeout) {
        uint16_t t = 0;
        const uint16_t tMax = microsecondsToClockCycles(timeout);
        while ((digitalRead(pin) != waitForValue) && (t < tMax)) {
            ++t;
        }
        return t;
    }

    void read() {
        // set the read flag to false in case we bail out of the function early
        h |= 0x80;

        // Go into high impedence state (pin is in input mode) to let pull-up raise data line level and start the reading process.
        digitalWrite(pin, HIGH);
        delay(250);
        // First set data line low for a delay greater than 18 ms
        pinMode(pin, OUTPUT);
        digitalWrite(pin, LOW);
        delay(20);
        // Then high for 40 ms - this ends the 'start signal'; also disable interrupts at this pointer
        noInterrupts();
        // use a do while false loop to make break statement available (instead of the naughty goto)
        do {
            digitalWrite(pin, HIGH);
            delayMicroseconds(40);
            // start reading now, ater a short wait for the sensor to pull the pin low
            pinMode (pin, INPUT_PULLUP);
            if (!waitRamp(pin, LOW, 70))
                break;
            // expect to receive two pulses: 80us low and then 80us high
            if (!waitRamp(pin, HIGH, 90))
                break;
            if (!waitRamp(pin, LOW, 90))
                break;
            // now read the data
            h  = readByte();
            hd = readByte();
            t  = readByte();
            td = readByte();
            uint8_t chk = readByte();
            if (chk == ((byte)(h + hd + t + td)))
                h |= 0x80;

            pinMode (pin, OUTPUT);
            digitalWrite (pin, HIGH);     // back to the high impedance mode
        } while(false);
        interrupts();
    }

    int getHumidity() const {
        return h & 0x7F;
    }

    int getTemperature() const {
        return t;
    }

    float getHumidityF() const {
        return 1.0f*h + 0.1f*hd;
    }

    float getTemperatureF() const {
        return 1.0f*t + 0.1f*td;
    }

    bool readOk() const {
        return (h & 0x80) > 0;
    }
};

#endif //TEMPERATURE_SENSOR_DHT11
