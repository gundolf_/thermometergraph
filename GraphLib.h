#ifndef GRAPH_LIB_H
#define GRAPH_LIB_H

#include <Arduino.h>

// depends on tha adafruit SSD1306 library
//#include <Adafruit_SSD1306.h>

/**
 * line graph plot (connected line) that is half the height of the display
 * Use:
 *  - either init with Adafruit SSD1306 & GFX libraries:
 *      Adafruit_SSD1306 display;
 *      LineGraph<Adafruit_SSD1306> graph(display);
 *  - or init with U8g2 library:
 *      U8G2_SSD1306_128X64_NONAME_1_4W_SW_SPI u8g2(...);
 *      U8g2GraphAdapter adapter(u8g2);
 *      LineGraph<Adafruit_SSD1306> graph(adapter);
 *  - add data:
 *      graph.addPoint(...); graph.addPoint(...); ...
 *  - plot onto the display buffer:
 *      graph.plot()
 *  - show on the display (Adafruit)
 *      display.display();
 *  - show on the display (U8g2lib)
 *      display.sendBuffer();
**/
template<class Display, size_t bufferSize = 128, class DataType = short>
class LineGraph {
    DataType data[bufferSize] = {0};
    unsigned int dataPosition = 0;
    unsigned int validDataPoints = 0;

  protected:
    Display& targetDisplay;

  public:
    LineGraph (Display& d) : targetDisplay (d) {}

    void addPoint (short int v) {
        data[dataPosition] = v;
        dataPosition++;
        validDataPoints++;
        if (validDataPoints > bufferSize) {
            validDataPoints = bufferSize;
        }
        if (dataPosition >= bufferSize) {
            dataPosition = 0;
        }
    }

    void plot (int x, int y, int w, int h) {
        targetDisplay.fillRect (x, y, w, h, 0);
        size_t ii = dataPosition >= validDataPoints ? 0 : dataPosition;
        for (size_t i = 1; i < validDataPoints; ++i) {
            size_t ii2 = ii + 1;
            if (ii2 >= bufferSize) {
                ii2 = 0;
            }
            //targetDisplay.drawLine(x+(i-1)*w/dataSize, y+data[ii]*h, x+i*w/dataSize, y+data[ii2]*h, WHITE);
            targetDisplay.drawLine (x + (i - 1)*w / bufferSize, y + data[ii], x + i * w / bufferSize, y + data[ii2], 1);
            ii = ii2;
        }
    }

    Display& getDisplayAdapter() { return targetDisplay; }
};

template<class Display, size_t bufferSize = 48, class DataType = short>
class LineGraphYAxis : LineGraph<Display, bufferSize, DataType> {
    typedef LineGraph<Display, bufferSize, DataType> LineGraphType;
    // scaling of points
    int16_t minimumVal, maximumVal;
  public:
    LineGraphYAxis (Display& d) : LineGraphType (d) {}

    void setMinMax (int16_t mm, int16_t mx) {
        minimumVal = mm;
        maximumVal = mx;
    }

    void add (int16_t v) {
        LineGraphType::addPoint (map (v, minimumVal, maximumVal, 31, 0));
        /*
        Serial.print("adding ");
        Serial.print(v);
        Serial.print(" (");
        Serial.print(map(v, minimumVal, maximumVal, 31, 0));
        Serial.print(" )\n");
        */
    }

    void plot() {
        char str[7];
        const int graphStartX = 16;
        LineGraphType::plot (graphStartX, 32, 128-graphStartX, 32);
        this->targetDisplay.setDrawColor (1);
        // make sure a nice font with max size of 11 (10 is best) is slected prior to calling plot()
        float delta = ((maximumVal - minimumVal) >> 4) / 8.0;

        for (int j = 0; j < 4; ++j) {
            sprintf (str, "%d", (int)round ((minimumVal >> 4) + delta * (7 - 2 * j)));
            this->targetDisplay.drawStr (0, 32 + 6 + 8 * j, str);
            for (int i = graphStartX; i < 128; i += 6) {
                this->targetDisplay.drawPixel (i + 3, 32 + 4 + 8 * j);
            }
        }
        /*

        sprintf(str, "%d", (int)((minimumVal >> 4) + delta*5));
        display.drawStr(0, 32 + 14, str);
        for (int i=graphStartX; i<128; i+=7)
            display.drawPixel(i+3, 32 + 3);
        sprintf(str, "%d", (int)((minimumVal >> 4) + delta*3));
        display.drawStr(0, 32 + 23, str);
        for (int i=graphStartX; i<128; i+=7)
            display.drawPixel(i+3, 32 + 3);
        sprintf(str, "%d", (int)((minimumVal >> 4) + delta));
        display.drawStr(0, 32 + 31, str);
        for (int i=graphStartX; i<128; i+=7)
            display.drawPixel(i+3, 32 + 3);
            */
    }
};

#endif //GRAPH_LIB_H
