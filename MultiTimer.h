#ifndef MULTI_TIMER_H
#define MULTI_TIMER_H

#include <Arduino.h>
#include "StaticLinkedList.h"

class MultiTimer : public StaticLinkedList <MultiTimer> {
    static long counter;
  protected:
    long nextInterrupt = 0;
  public:
    static void isrFunc() {
        counter++;
        // loop through all the registered timers
        MultiTimer* e = MultiTimer::getStartingListElement();
        for (; e != nullptr; e = e->getNextListElement()) {
            if (e->nextInterrupt == counter)
                if (e->onTimerCheck (counter)) {
                    e->onTimer ();
                }
        }
    }

    MultiTimer () {
        this->addToListStart (this);
    }

    ~MultiTimer () {
        this->removeFromList (this);
    }

    /**
     * Set new time (relative to this moment) for waking up; The timeout parameter will be clamped to the values > 0
     **/
    void setRelative (long timeout) {
        noInterrupts();
        // this function may never fail to set the next wakeup interrupt, even if negative number is provided
        nextInterrupt = counter + (timeout > 0 ? timeout : 1);
        interrupts();
    }

    virtual bool onTimerCheck (long)    { return true; }
    virtual void onTimer()              {}

    long getCounter() const {
        return counter;
    }

    long getTimeout() const {
        return nextInterrupt - counter;
    }
};

class RepeatableMultiTimer : public MultiTimer {
    long repeatEvery = 0;
  public:
    /**
     * Initialize the timer object for infinite waiting; setPeriod should be called afterwards o end he infinite wait.
     **/
    RepeatableMultiTimer() {}

    /**
     * Initialize the timer with initial timeout and period for repeated waking up.
     * Note that the initial timoout must not be lower than 1, and will be clamped to 1 if invalid value is supplied.
     **/
    RepeatableMultiTimer (long initialTimeout, long period) : repeatEvery (period) {
        setRelative (initialTimeout);
    }

    /**
     * Sets or resets the period for repeated waking up. If the period was not set up before then this period
     * will be used to set up both period and next wakeup timeout; If the period has been setup before then
     * the new period will be used as the timer period, but the next wakeup timeout will be calculated as the
     * difference between the newly setup period and the already waied time in current period
     **/
    void setPeriod (long period) {
        // first clip the period to legal values
        if (period <= 0) {
            period = 1;
        }

        // now set up timeout and period
        if (repeatEvery == 0) {
            repeatEvery = period;
            setRelative (period);
        } else {
            long temp = nextInterrupt - repeatEvery + period;
            repeatEvery = period;
            setRelative (temp);
        }
    }

    /**
     * Debugging fascility: checks weather the timer is still alive, that is was setup and its next wakeup time is
     * valid (that is it is in the future); It will return false for the timers that were not set up yet
     **/
    bool isAlive() const {
        // == may occur only if this is triggered in the middle of the timer ISR
        return (nextInterrupt >= getCounter());
    }

    /**
     * Query weather the timer has been setup yet
     **/
    bool isSetup() const {
        return (repeatEvery > 0);
    }

    /**
     * enable or disable the repeating wakeups
     **/
    void enable(bool en = true) {
        // enable by setting timer period to a positive value, disable by making it negative
        repeatEvery = en ? abs(repeatEvery) : abs(repeatEvery);
        // if enable was requested then also make sure it is alive
        if (en && !isAlive()) {
            setRelative(1);
        }
    }

    /**
     * Query weather the timer has been setup yet
     **/
    long getTimerPeriod() const {
        return abs(repeatEvery);
    }

  protected:
    /**
     * Thi is not a user function, it is rather called by MultiTimer2 to check weather to trigger onTimer or not.
     **/
    bool onTimerCheck (long cnt) {
        // register a repeat wakeup
        setRelative (repeatEvery);
        // trigger on Timer only if it was setup yet
        return isSetup();
    }
};


/**
 * A class for executing tasks in a synchronous manner; like a handler.
 * This class goes hand in hand with MultiTimer; To avoid long processing inside the timer ISR, Multitimer enables
 * a tash and tasker then executes it in side main (or loop) funciton.
 **/
class MultiTasker : public StaticLinkedList <MultiTasker> {
  protected:
  public:
    static void run() {
        MultiTasker* e = getStartingListElement();
        for (; e != nullptr; e = e->getNextListElement()) {
            if (e->isReady()) {
                e->onExecute();
            }
        }
    }

    MultiTasker () {
        this->addToListStart (this);
    }

    ~MultiTasker () {
        this->removeFromList (this);
    }

    virtual bool isReady() = 0;
    virtual void onExecute() = 0;
};

template<class TimerType = RepeatableMultiTimer>
class SimpleTimerTask : public MultiTasker, public TimerType {
    bool ready = false;

public:
    void setReady()     { ready = true; }
    void onTimer()      { ready = true; }
    bool isReady()      { if (ready) { ready = false; return true; } else return false; }
};

#endif //MULTI_TIMER_H
