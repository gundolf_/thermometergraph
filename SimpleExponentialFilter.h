#ifndef SIMPLE_EXPONENTIAL_FILTER_H
#define SIMPLE_EXPONENTIAL_FILTER_H

class SimpleExponentialFilter {
    float alpha;
    float accum = 0;
public:
    SimpleExponentialFilter(float alpha) : alpha(alpha) {}

    float setTo(float num) {
        return accum = num;
    }

    float filter(float sample) {
        accum = accum * alpha + sample * (1-alpha);
        return accum;
    }

    float get() const {
        return accum;
    }
};

class IntExponentialFilter {
    int alpha, unit;
    long accum = 0;
public:
    IntExponentialFilter(int alpha, int unit) : alpha(alpha), unit(unit) {}

    void setTo(int num) {
        accum = num;
    }

    int filter(int sample) {
        accum = (accum * alpha + sample * (unit - alpha) + (unit >> 1))/unit;
        return accum;
    }
};

#endif //SIMPLE_EXPONENTIAL_FILTER_H
