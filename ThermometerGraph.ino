#include <Arduino.h>
// use c++ style character streaming (besides being user friendly it also uses less RAM & flash)
#include <Streaming.h>
#include "CharStream.h"
// for storing to EEPROM
#include <EEPROM.h>
// include Wire and SPI are used to help U8g2lib compile (adds the required library path)
#include <Wire.h>
#include <SPI.h>
//#include <EEPROM.h>
// Timer library for performing repeated checks on input
#include <FlexiTimer2.h>
// graphics library for driving the small OLED
#include <U8g2lib.h>
// my libs
#include "GraphLib.h"
#include "U8g2GraphAdapter.h"
#include "PotJoystick.h"
#include "TemperatureSensor_DHT11.h"
#include "SimpleExponentialFilter.h"
#include "MultiTimer.h"
// fixed point definitions
#include "FixedPoint12dot4.h"
#include "JoystickControlledUI.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// comment and uncomment the defines within this block of code to test various parts of the system
//#define COMM_TEST
//#define TIMER_DEBUG
#define DHT11_ENABLED
//#define DHT11_TEST
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

// the following statements define a friendly way of writing conditionally compiled code
#if (!defined COMM_TEST && !defined DHT11_TEST)
    #define FULL_PROJECT
    #define IF_FULL_PROJECT(PERFORM) do { PERFORM; } while (false)
#else
    #define IF_FULL_PROJECT(PERFORM) {}
#endif

// COMM_TEST macro usage: COMM_TEST { Serial << "This will be executed only in COMM_TEST mode" }; }
#ifdef COMM_TEST
    #define IF_COMM_TEST(PERFORM) do { PERFORM; } while (false)
#else
    #define IF_COMM_TEST(PERFORM) {}
#endif

// define when UI on the OLED is enbled
#if (defined FULL_PROJECT)
    #define UI_ENABLED
    #define IF_UI_ENABLED(PERFORM) do { PERFORM; } while (false)
#else
    #define IF_UI_ENABLED(PERFORM) {}
#endif


// OLED on the I²C:         link pin A5 to CLK on the OLED and pin A4 to SDA on the OLED
U8G2_SSD1306_128X64_NONAME_2_HW_I2C display (U8G2_R0, U8X8_PIN_NONE); // parameters are: rotation, pin RESET
// adapter object to be used for graphs
U8g2GraphAdapter adapter (display);

/**
 * Structure representing the sensor value; composed of the fixed point final value and adjustment value.
 * Raw value should be passed through the setRaw function, where it will be adjusted by the adjustment value.
 * Adjustment vlaue has 8 bit resolution (7 bit int, 1 bit fraction)
 */
struct SensorValue : public FixedPoint12dot4 {
    int8_t adjust;

    /**
     * set raw value of the sensor (this can also be a filtered value); adjustment will be applied in this function
     * @param rawData raw data as integer (e.g. the value multiplied by 16)
     */
    void setRaw(int16_t rawData) {
        FixedPoint12dot4::setRaw(rawData + (adjust << 3));
    }
};

// here are the observed global sensor variables - the data around which this whole project is built
SensorValue insideTemperature;
SensorValue outsideTemperature;
SensorValue insideHumidity;
SensorValue outsideHumidity;

// All graph objects in this project will be of the same type, define this type here:
// Line graph with: Y axis marked, 48 values on the x axis, uint8_t type for storing y values
typedef LineGraphYAxis<U8g2GraphAdapter, 48, int8_t> LinePlot;

template<char headerValue>
struct EepromStruct {
    char header;

    /**
     * After reading the structure, check if it is valid; if the structure is read before written to EEPROM for the
     * first time, this function will return false. There is a small chance it will return true if the EEPROM
     * accidentally contains the exact header value on just the right address (this should be rare and happen only
     * if EEPROM was previously used for something else; not on the very first use of the given EEPROM address).
     * @return true if the structure has been read successfully
     */
    bool isValid() const {
        return headerValue == header;
    }

    void initForWriting() {
        header = headerValue;
    }
};

/**
 * structure that holds joystick initialization in EEPROM
 */
struct JoystickEpromStruct : public EepromStruct<'j'> {
    uint8_t activeState;    // either HIGH or LOW: whichecer button is active in
    int cx;                 // central joystick position x axis
    int cy;                 // central joystick position y axis
    int range;              // range of joystick movement
};

/**
 * Structure that contains sensors (all of them) calibration in the EEPROM
 */
struct SensorCalibrationStruct : public EepromStruct<'c'> {
    int8_t  inT;
    int8_t  inH;
    int8_t  outT;
    int8_t  outH;
};

// Specify the EEPROM addresses here; first address = 0, all subsequent: address = previous address + sizeof(previous struct)
const int joystickEepromAddress             = 0;
const int sensorCalibrationEepromAddress    = joystickEepromAddress + sizeof(JoystickEpromStruct);

/**
 * Helper function for storing the adjustment value from known sensors to the sensorCalibration structure in EEPROM
 * @param sensor the sensor that has been adjusted and is to be stored
 */
void storeSensorAdjustment(SensorValue& sensor) {
    Serial << F("Storing adjustment ") << sensor.adjust << '\n';
    // load the calibration structure
    SensorCalibrationStruct sc;
    // read values first to init sensor calibration struct
    EEPROM.get(sensorCalibrationEepromAddress, sc);
    sc.initForWriting();
    // determine which sensor is being manipulated by comparing the sensor address to all known sensor addresses;
    if (&sensor == &insideTemperature)
        sc.inT = sensor.adjust;
    if (&sensor == &insideHumidity)
        sc.inH = sensor.adjust;
    if (&sensor == &outsideTemperature)
        sc.outT = sensor.adjust;
    if (&sensor == &outsideHumidity)
        sc.outH = sensor.adjust;
    // store the modified adjustment
    EEPROM.put(sensorCalibrationEepromAddress, sc);
}

/**
 * Helper function to load adjustment values for all sensors. This should be called in the setup.
 */
void loadSensorAdjustments() {
    // load the calibration structure
    SensorCalibrationStruct sc;
    EEPROM.get(sensorCalibrationEepromAddress, sc);
    // if this is an invelid sensor calibration structure (likely because it was never written to EEPROM before) then initialize adjustments with zeros
    if (!sc.isValid()) {
        sc.inT = 0;
        sc.inH = 0;
        sc.outT = 0;
        sc.outH = 0;
    }
    insideTemperature.adjust = sc.inT;
    insideHumidity.adjust = sc.inH;
    outsideTemperature.adjust = sc.outT;
    outsideHumidity.adjust = sc.outH;
    Serial << F("Loaded sensor adjustment values: ") << sc.inT << ", " << sc.inH << ", " << sc.outT << ", " << sc.outH;
    if (!sc.isValid())
        Serial << F(" !");
    Serial << "\n";
}


/**
 * This class represents the main part of the user interface. Should have only one instance.
 * Although I would like to have this class in its own file, it is oo impractical, since it uses lots of global variables.
 */
struct UserInterface {
    JoystickControlledUI joystickInterface;
    // define buffer for forming strings on the fly (making it local does not make sense)
    CharStream<20> strBuffer;           // 20 characters is about the most that fits into a single line on OLED big-font text that fits into single line of OLED
    // another buffer for when 2 lines of text are displayed
    CharStream<15> smallStrBuffer;
    // index of the active screen
    int8_t activeScreenIndex = 0;
    // index of the active sub-screen
    int8_t activeSubScreenIndex = 0;
    // keep record of what was screen was last shown, so that change can be detected and acted upon
    int8_t lastShownScreen = activeScreenIndex;
    // temp integer, target variable for the joystick movement
    int8_t tempAdjustTarget = 0;
    // enums of all the screens
    enum {
        screen_joystick_init,
        screen_graphLocalTemp,
        screen_graphLocalHumidity,
        screen_graphRemoteTemp,
        screen_graphRemoteHumidity,
        screen_Modbus_stats,
        number_of_screens
    };
    // subscreens of the joystick calibrate screen
    enum {
        joystick_calibration_confirm,
        joystick_calibration_start,
        joystick_calibration_tilt,
        joystick_calibration_done,
        joystick_calibration_wait_exit,
        number_of_subscreens
    };
    // graphs for local/remote temperature/humidity; local graphs come in two flavors - normal and fast
    LinePlot insideTempGraph, insideHumidityGraph, outsideTempGraph, outsideHumidityGraph, fastTempGraph, fastHumidityGraph;
    // A timer for temporary timings. MultiTimer is used, because it can be used in 'passive' mode - onTimer function will do nothing, but its timeout can be checked manually
    MultiTimer dummyTimer;

    UserInterface (PotJoystick& j) :
        joystickInterface (j), insideTempGraph (adapter), insideHumidityGraph (adapter),
        outsideTempGraph (adapter), outsideHumidityGraph (adapter), fastTempGraph (adapter), fastHumidityGraph (adapter)
    {
        // initialize the default screen and subscreen - they do not need to be 0
        activeScreenIndex = 0;
        activeSubScreenIndex = 0;

        // set up graphs; the limits should be specified in FixedPoint12dot4 raw values, that is, shifted left by 4
        insideTempGraph.setMinMax (10 << 4, 30 << 4);        // local emperature expected range is 10 - 30 degrees Celsius
        outsideTempGraph.setMinMax (-20 << 4, 40 << 4);      // remote temperature here is rarely not between -20 and 40
        insideHumidityGraph.setMinMax (0 << 4, 100 << 4);    // relative humidity exressed in percents: between 0 and 100
        outsideHumidityGraph.setMinMax (0 << 4, 100 << 4);   // again relative humidity in percents
        fastTempGraph.setMinMax (10 << 4, 30 << 4);         // fast local graph should have the same limits as insideTempGraph
        fastHumidityGraph.setMinMax (0 << 4, 100 << 4);     // relative humidity in percents again
    }

    /**
     * This function should be called to the UI in the setup part of the Arduino program
     */
    void start() {
        // determine if the joystick has been calibrated yet
        JoystickEpromStruct js;
        EEPROM.get(joystickEepromAddress, js);

        // on the first run, when no calibration is stored yet, trigger a new calibration, on other runs, load the stored calibration
        if (js.isValid()) {
            joystickInterface.getPotJoystick().externalInit(js.cx, js.cy, js.range, js.activeState == LOW);
            // move to the first screen and enable joystick navigation of the screens
            activeScreenIndex = screen_graphLocalTemp;
            enableScreenNavigation();
        } else {
            startJoystickCalibration();
        }
    }

    /**
     * Show/display the active screen; this function should be called several times per second or at every change on the user interface
     * This function will redraw the contents of the OLED display
     */
    void showScreen() {
        // activeScreenIndex and subScreenIndex may change while drawing, so make copies here
        // disable interrupts, bound indices and and then copy them, then enable interrupts again
        noInterrupts();
        // limit the screen index to the range of valid screens and make it roll around (negative values are not allowed)
        // first, force the active screen index to range between (-number_of_screens+1) and (number_of_screens-1)
        activeScreenIndex = activeScreenIndex % number_of_screens;
        // then fix the negative screen numbers
        if (activeScreenIndex < 0) {
            activeScreenIndex += number_of_screens;
        }
        // make a copy of the variable
        int8_t activeScreen = activeScreenIndex;
        // reset the subscreen to 0 if the screen has changed
        if (activeScreen != lastShownScreen) {
            activeSubScreenIndex = 0;
        }
        // constrain the subscreen index; this part here although this violates the rule of putting the code for one task at one place but
        // having this code here saves some RAM and FLASH; the rest of subscreen index handlng is done separately in functions that handle different screens
        switch (activeScreen) {
        case screen_graphLocalTemp:
        case screen_graphLocalHumidity:
            // local humidity and temperature have 3 subscreens: calibration, graph, and graph for shorter time interval
            activeSubScreenIndex = constrain(activeSubScreenIndex, -1, 1);
            break;
        case screen_graphRemoteTemp:
        case screen_graphRemoteHumidity:
            // remote temperature and humidity have 2 subscreens: calibration and graph
            activeSubScreenIndex = constrain(activeSubScreenIndex, -1, 0);
            break;
        case screen_Modbus_stats:
            activeSubScreenIndex = 0;
        }
        int8_t activeSubScreen = activeSubScreenIndex;
        // remember what was just shown
        lastShownScreen = activeScreen;
        interrupts();

        // act on screen change here
        if (activeScreen != lastShownScreen) {
            // a bit of defensive programming - make sure screen navigation is enabled
            enableScreenNavigation();
            // if on the joystick calibration screen, then disable the sub-screen joystick navigation (because it is implemented differently on that screen)
            if (activeScreen == screen_joystick_init)
                joystickInterface.yVariable = nullptr;
        }

        // draw the active screen
        switch (activeScreen) {
        case screen_graphLocalTemp:
            strBuffer.start() << F("In T ") << insideTemperature.print();
            strBuffer.print("aaa");
            displayGraph(insideTempGraph, fastTempGraph, insideTemperature, activeSubScreen);
            break;
        case screen_graphLocalHumidity:
            strBuffer.start() << F("In RH ") << insideHumidity.print();
            displayGraph (insideHumidityGraph, fastHumidityGraph, insideHumidity, activeSubScreen);
            break;
        case screen_graphRemoteTemp:
            strBuffer.start() << F("Out T ") << outsideTemperature.print();
            displayGraph (outsideTempGraph, outsideTempGraph, outsideTemperature, activeSubScreen);
            break;
        case screen_graphRemoteHumidity:
            strBuffer.start() << F("Out RH ") << outsideHumidity.print();
            displayGraph (outsideHumidityGraph, outsideHumidityGraph, outsideHumidity, activeSubScreen);
            break;
        case screen_Modbus_stats:
            displayModBusStats();
            break;
        case screen_joystick_init:
            processCalibrateJoystickScreen();
            break;
        default:
            Serial << F("Show screen without a valid screen: ") << (activeScreen) << '\n';
        }
    }

  private:
    /**
    * Tell the UI to use joystick for screen navigation (left/right - move between screens; up/down - move between sub-screens)
    */
    void enableScreenNavigation() {
        joystickInterface.xVariable = &activeScreenIndex;
        joystickInterface.yVariable = &activeSubScreenIndex;
    }

    /**
     * Display a graph in the lower half of the screen
     **/
    void displayGraph (LinePlot& g, LinePlot& altG, SensorValue& sensor, int8_t& activeSubScreen) {
        // if button was pressed, act upon it
        if (joystickInterface.getButonReleasedFlag()) {
            joystickInterface.acknowledgeButtonReleasedFlag();
            // start calibration if on calibration screen
            if (activeSubScreen == -1) {
                // is adjusting enabled?
                if (joystickInterface.yVariable != nullptr) {
                    // no: point the joystick x axis tilt control to a temporary adjust variable, and y tilt control to nothing
                    tempAdjustTarget = sensor.adjust;
                    joystickInterface.xVariable = &tempAdjustTarget;
                    joystickInterface.yVariable = nullptr;
                } else  {
                    // yes; exiting the adjustment/calibration screen, re-enable navigation and store the adjustment to EEPROM
                    enableScreenNavigation();
                    storeSensorAdjustment(sensor);
                }
            } else {
                // just in case something bad happened, link button press to enabling screen navigation (nothing worse than getting stuck in a creen with no way of navigating out)
                enableScreenNavigation();
            }
        }

        // draw preprocess
        if (activeSubScreen == -1) {
            // update the sensor value if necessary
            if (joystickInterface.yVariable == nullptr) {
                if (tempAdjustTarget != sensor.adjust) {
                    sensor.setRaw(sensor.value + ((tempAdjustTarget - sensor.adjust - sensor.adjust) << 3));
                    sensor.adjust = tempAdjustTarget;
                }
            } else {
                tempAdjustTarget = sensor.adjust;
            }
            // a contrieved way of properly outputting positive and negative decimal values from integer input:
            smallStrBuffer.start();
            // if adjusting is active, add a message on screen
            if (joystickInterface.yVariable == nullptr)
                smallStrBuffer << F("Adjust ");
            smallStrBuffer << (sensor.adjust >= 0 ? ' ' : '-') << (abs(sensor.adjust) / 2) << '.' << (sensor.adjust & 1)*5;
        } else {
            // prepare the string that shows graph mode (slow 24h mode or fast 20 minute mode)
            smallStrBuffer.start() << (activeSubScreen == 0 ? F("24h ") : F("20m "));
        }
        // draw loop
        display.firstPage();
        do {
            if (activeSubScreen == -1) {
                // calibrate sensor screen
                display.setDrawColor (1);
                display.setFont (u8g2_font_helvB12_tf);
                display.drawStr (0, 24, strBuffer);
                display.drawStr (0, 24+32, smallStrBuffer);
            } else {
                // graph screen
                display.setDrawColor (1);
                display.setFont (u8g2_font_helvB12_tf);
                display.drawStr (0, 24, strBuffer);
                display.drawStr (128-31, 24, smallStrBuffer.str());
                display.setFont (u8g2_font_profont10_mn);
                if (activeSubScreen == 0) {
                    g.plot ();
                } else {
                    altG.plot ();
                }
            }
        } while ( display.nextPage() );
    }

    void startJoystickCalibration() {
        // disable joystick movement link to the UI
        joystickInterface.xVariable = 0;
        joystickInterface.yVariable = 0;
        // just in case, assert the right screen is selected
        activeScreenIndex = screen_joystick_init;
        // activeSubScreenIndex will hold the state of calibration
        activeSubScreenIndex = joystick_calibration_start;
        // feed the inialization with invalid values;
        joystickInterface.getPotJoystick().externalInit (-1, -1, -1);
        // start the dummy timer to countdown until the center position is read
        dummyTimer.setRelative (3500L);
    }

    void processCalibrateJoystickScreen () {
        auto joystickState = joystickInterface.getPotJoystick().readState();
        int t = dummyTimer.getTimeout();

        // first check if the activeSubScreenIndex should be switched (not the best place to do it but it's here to keep the code simple)
        switch (activeSubScreenIndex) {
        case joystick_calibration_confirm:
            // if button is pressed & released then switch to the next state
            if (joystickState.buttonReleasedFlag()) {
                joystickInterface.acknowledgeButtonReleasedFlag();
                startJoystickCalibration();
                activeSubScreenIndex = joystick_calibration_start;
            }
            break;
        case joystick_calibration_start:
            // when the timeout is over, switch to the next state
            if (t < 0) {
                joystickInterface.getPotJoystick().initCentralPosition();
                activeSubScreenIndex = joystick_calibration_tilt;
            }
            break;
        case joystick_calibration_tilt:
            // if button is pressed & released after the joystick has been tilted enough, switch to the next state
            if ((joystickInterface.getPotJoystick().getMovementRange() > 50) && (joystickState.buttonReleasedFlag())) {
                joystickInterface.acknowledgeButtonReleasedFlag();
                activeSubScreenIndex = joystick_calibration_done;
            }
            break;
        case joystick_calibration_done:
            // if button is pressed & released after the joystick has been tilted enough, switch to the next state
            if (joystickState.buttonReleasedFlag()) {
                joystickInterface.acknowledgeButtonReleasedFlag();
                activeSubScreenIndex = joystick_calibration_wait_exit;
                // set a countdown to make reentering the calibration on accident less likely
                dummyTimer.setRelative (3000L);
                // store the calibration to EEPROM
                JoystickEpromStruct js;
                js.initForWriting();
                joystickInterface.getPotJoystick().getInitValues(js.cx, js.cy, js.range);
                js.activeState = joystickInterface.getPotJoystick().activeState();
                EEPROM.put(joystickEepromAddress, js);
                // turn the joystick capture back to the ui
                enableScreenNavigation();
            }
            break;
        case joystick_calibration_wait_exit:
            if (t < 0) {
                activeSubScreenIndex = joystick_calibration_confirm;
            }
            break;
        default:
            break;
        }

        showCalibrateJoystickScreen(joystickState, t);
    }

    void showCalibrateJoystickScreen (PotJoystick::State& joystickState, int t) {
        // display loop
        display.firstPage();
        do {
            // subscreens are handled in the following switch
            switch (activeSubScreenIndex) {
            case joystick_calibration_confirm:
                display.setDrawColor (1);
                display.setFont (u8g2_font_helvB12_tf);
                strBuffer.start() << F("Calibrate");
                display.drawStr (0, 24, strBuffer);
                strBuffer.start() << F("Joystick?");
                display.drawStr (0, 48, strBuffer);
                break;
            case joystick_calibration_start:
                display.setDrawColor (1);
                display.setFont (u8g2_font_helvB12_tf);
                strBuffer.start() << F("Center joystick");
                display.drawStr (0, 24, strBuffer);
                strBuffer.start() << (t / 1000) << '.' << ((t / 100) % 10) << F(" s left");
                display.drawStr (0, 48, strBuffer);
                break;
            case joystick_calibration_tilt:
                display.setDrawColor (1);
                display.setFont (u8g2_font_helvB12_tf);
                strBuffer.start() << F("Tilt joystick");
                display.drawStr (0, 24, strBuffer);
                strBuffer.start() << F("range = ") << joystickInterface.getPotJoystick().getMovementRange();
                display.drawStr (0, 48, strBuffer);
                break;
            case joystick_calibration_done:
            case joystick_calibration_wait_exit:
                display.setDrawColor (1);
                display.setFont (u8g2_font_helvB12_tf);
                strBuffer.start() << F("x: ") << (int)joystickState.getRelativeX() << ' ' << '%';
                display.drawStr (0, 24, strBuffer);
                strBuffer.start() << F("y: ") << (int)joystickState.getRelativeY() << ' ' << '%';
                display.drawStr (0, 48, strBuffer);
                break;
            default:
                break;
            }
        } while ( display.nextPage() );
    }

    /**
     * Display stats for modbus driver (for troubleshooting only)
     */
    void displayModBusStats() {
        display.setDrawColor (1);
        display.setFont (u8g2_font_helvB12_tf);
        display.firstPage();
    }
};

// Joystick, the main input device
PotJoystick joystick (A0, A1, 3);
#ifdef UI_ENABLED
// User interface - tha part that will take care of the screen
UserInterface userInterface (joystick);
#endif

#ifdef DHT11_ENABLED
/**
 * Timer-based reader of the dht11
 * Will read the sensor every 10 seconds and then do some heavy filtering on the raw values
 **/
struct Dht11Reader : SimpleTimerTask<> {
    // thermometer/hygrometer DHT11: the internal temperature and humidity provider
    TemperatureSensor_DHT11 dht11;
    // filter
    SimpleExponentialFilter filterT;
    SimpleExponentialFilter filterH;

    Dht11Reader() : dht11(2), filterT (0.95), filterH (0.95) {
        // 7 is taken as a silly enough number, not related to 25 (fast graphd take a sample every 25 seconds) for nicer graphs
        setRelative(7);
        setPeriod(10000L);
        filterT.setTo (-9999);
        filterH.setTo (-9999);
    }

    void onExecute() {
        dht11.read();
#ifdef DHT11_TEST
        Serial << F("DHT11 reading: ");
        if (!dht11.readOk()) {
            Serial << "!";
        }
        Serial << (dht11.getTemperature()) << '\n';
#endif
        if ((filterT.get() == -9999) && dht11.readOk()) {
            insideTemperature.setRaw (filterT.setTo (dht11.getTemperature() << 4));
            insideHumidity.setRaw (filterH.setTo (dht11.getHumidity() << 4));
        } else {
            insideTemperature.setRaw (round (filterT.filter (dht11.getTemperature() << 4)));
            insideHumidity.setRaw (round (filterH.filter (dht11.getHumidity() << 4)));
        }
    }
};
Dht11Reader dht11Reader;
#endif // DHT11_ENABLED

/**
 * This class will take care of adding the latest sensor readings to the graphs
 * It is setup to trigger every 20 minutes
 **/
struct GraphDataAdder : SimpleTimerTask<> {
    GraphDataAdder() {
        setRelative(1000);
        // repeat every 30 minutes: 30 minutes * 60 seconds * 1000 millis
        setPeriod(30 * 60 * 1000L);
    }

    void onExecute() {
        #ifdef UI_ENABLED
        userInterface.insideTempGraph.add (insideTemperature.value);
        userInterface.insideHumidityGraph.add (insideHumidity.value);
        userInterface.outsideTempGraph.add (outsideTemperature.value);
        userInterface.outsideHumidityGraph.add (outsideTemperature.value);
        #endif
    }
};
GraphDataAdder graphDataAdder;

/**
 * This class will take care of adding the latest sensor readings to the graphs
 * It is setup to trigger every minute
 **/
struct GraphFastAdder : SimpleTimerTask<> {
    GraphFastAdder() {
        // repeat every 25 seconds
        setPeriod(25L * 1000);
    }

    void onExecute() {
        #ifdef UI_ENABLED
        userInterface.fastTempGraph.add (insideTemperature.value);
        userInterface.fastHumidityGraph.add (insideHumidity.value);
        #endif
    }
};
GraphFastAdder graphFastAdder;

#ifdef MODBUS_ENABLED
class ModBusTask : SimpleTimerTask<> {
  public:
    ModBusTask() {
        modBusMaster.init();
        // communicate only once per second
        setPeriod (1000L);
    }

    void onExecute() {
        modBusMaster.setInternalTempAndHumidity(insideTemperature.getInt(), insideHumidity.getInt());
        // execute communication; no need to check result it will be automatic in the next call
        IF_MODBUS_DEBUG(Serial.println(F("ModBus execute start")));
        modBusMaster.execute();
        IF_MODBUS_DEBUG(Serial.println(F("ModBus execute end")));
        uint8_t t = outsideTemperature.getInt();
        uint8_t rh = outsideHumidity.getInt();
        modBusMaster.getExternalTempAndHumidity(t, rh);
        outsideTemperature.setInt(t);
        outsideHumidity.setInt(rh);
    }
};
ModBusTask modBusTask;
#endif

#ifdef TIMER_DEBUG
/**
 * This class will implement a timer with 1 second resolution that will be able to output its value to Serial or
 * somewhere else.
 */
class TimerTask : SimpleTimerTask<> {
    int cnt = 0;

  public:
    TimerTask (int period) {
        setPeriod (period * 1000L);
    }

    void onExecute() {
        ++cnt;
        Serial << F("Timer ");
        Serial.println (cnt);
    }
};
TimerTask dt1(1); //, dt2(5), dt3(60);
#endif

struct SerialData {
    CharStream<5> serialDataBuffer;
    static const char dataHeader = '\b';

    void checkForSerialData() {
        // calling Serial.read when nothing is in buffer would block the execution, therefore check that the buffer has something to read before reading
        while (Serial.available()) {
            char ch = Serial.read();
            // data starts with dataHeader character
            if (ch == dataHeader) {
                serialDataBuffer.start();
                continue;
            }
            // data ends with newline character
            if ((ch == '\n') && (serialDataBuffer.getNumFree() > 0)) {
                serialDataBuffer.end();
                onDataReceived();
                // protect the buffer against writing to it (serves as a kind of a flag that the characters are not to be accepted)
                serialDataBuffer.rewindToEnd();
            } else {
                // only add character if there s enough space in the buffer
                if (serialDataBuffer.getNumFree() > 1) {
                    serialDataBuffer << ch;
                }
            }
        }
    }

private:
    /**
     * This function is called automatically whenever data is received
     */
    void onDataReceived() {
        // just assume that 4 bytes of data are received here, first two are temperature in fixed point format (data is not sent as text!),
        // the 3rd and 4th are humidity in the same format
        outsideTemperature.setRaw(*(int16_t*)(serialDataBuffer.str()));
        outsideHumidity.setRaw(*(int16_t*)(&serialDataBuffer.str()[2]));
//        IF_COMM_TEST({
        Serial << F("raw data received: [") << (int)serialDataBuffer.str()[0] << ',' << (int)serialDataBuffer.str()[1] << ',' << (int)serialDataBuffer.str()[2] << ',' << (int)serialDataBuffer.str()[3];
        Serial << F("]; T=") << outsideTemperature.print() << F(", RH=") << outsideHumidity.print() << '\n';
//        });
    }
};

SerialData serialData;

void setup() {
    FlexiTimer2::set (1, MultiTimer::isrFunc);
    FlexiTimer2::start();

    Serial.begin (9600);
    Serial.println (F("\nHello from the climate monitoring centre\n"));

    // setup joystick
    joystick.setupPins();
    //joystick.initCentralPosition();

    // user interface output
    display.begin();
    // see https://github.com/olikraus/u8g2/wiki/fntlistall for available fonts
    display.setContrast (0);

    IF_UI_ENABLED(userInterface.start());
    loadSensorAdjustments();
}

void loop() {
    // the loop for multitasker (manager of tasks based on timer) and user interface (could be implemented as task too...)
    MultiTasker::run();
    IF_UI_ENABLED(userInterface.showScreen());
    serialData.checkForSerialData();
    delay (100);
}
