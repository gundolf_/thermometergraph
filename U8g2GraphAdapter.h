#ifndef U8G2_GRAPH_ADAPTER_H
#define U8G2_GRAPH_ADAPTER_H
#include <U8g2lib.h>

class U8g2GraphAdapter {
    U8G2& target;

  public:
    U8g2GraphAdapter (U8G2& targetU8g) : target (targetU8g) {}

    void fillRect (int16_t x, int16_t y, int16_t x2, int16_t y2, uint8_t color) {
        target.setDrawColor (color);
        target.drawBox (x, y, x2 - x, y2 - y);
    }

    void drawBox (int16_t x, int16_t y, int16_t x2, int16_t y2) {
        target.drawBox (x, y, x2, y2);
    }

    void drawLine (int16_t x, int16_t y, int16_t x2, int16_t y2, uint8_t color) {
        target.setDrawColor (color);
        target.drawLine (x, y, x2, y2);
    }

    void setDrawColor (uint8_t color) {
        target.setDrawColor (color);
    }

    void drawStr (int x, int y, const char* str) {
        target.drawStr (x, y, str);
    }

    void drawPixel (int x, int y) {
        target.drawPixel (x, y);
    }

};

#endif //U8G2_GRAPH_ADAPTER_H
