#ifndef STATIC_LINKED_LIST_H
#define STATIC_LINKED_LIST_H

#include <Arduino.h>

template<class T>
struct StaticLinkedList {
    static StaticLinkedList* head;
    StaticLinkedList* next = nullptr;
    void addToListStart (StaticLinkedList* element) { element->next = head; head = element; }
    T* getNextListElement() {return static_cast<T*> (next);}
    static T* getStartingListElement() {return static_cast<T*> (head);}
    /**
     * This function is untested!
     **/
    static void removeFromList(T* element) {
        StaticLinkedList* e = head;
        StaticLinkedList* prevE = nullptr;
        for (; e != nullptr; e = e->next) {
            if (e == element) {
                if (prevE != nullptr)
                    prevE->next = e->next;
                else
                    head = e->next;
            }
            prevE = e;
        }
    }
};

template<class T>
StaticLinkedList<T>* StaticLinkedList<T>::head = nullptr;

#endif //STATIC_LINKED_LIST_H
