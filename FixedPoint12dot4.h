#ifndef FIXED_POINT_12_DOT_4_H
#define FIXED_POINT_12_DOT_4_H

#include <Arduino.h> // for the uint8_t and similar type definitions


/**
 * An encapsulation for small fixed point numbers. 12 bits for int part (∓2000) and 4 bits for fractions (¹/₁₆ resolutions)
 * It is meant for storing temperature and humidity values. It should easily ouperform floats for these quantities.
 */
struct FixedPoint12dot4 {
    /**
     * A smal ltrade-off was chosen, to make the Printable nterface a separate class (FixedPoint12dot4 could inherit
     * from Printable directoly), to make FixedPoint12dot4 class more lightweight and to keep it POD. If it inherited
     * an abstract class, it's size would increase by one pointer etc.
     * Therefore to print an object 'f' of class FixedPoint12dot4, use f.print(); e.g.: Serial.print(f.print());
     */
    struct PrintableFixedPoint12dot4 : public Printable {
        friend class FixedPoint12dot4;
        const FixedPoint12dot4& ref;

        PrintableFixedPoint12dot4(const FixedPoint12dot4& ref) : ref(ref) {}

        size_t printTo(Print& stream) const override {
            size_t totalSize = 0;
            if (ref.isNegative() && (ref.getInt() == 0))
                totalSize += stream.print('-');
            totalSize += stream.print(ref.getInt());
            totalSize += stream.print('.');
            totalSize += stream.print(ref.getFrac());
            return totalSize;
        }
    };

    int16_t value;

    /**
     * Query if the number is negative (this function is mostly used when converting the numbers to text)
     * @return true if the number is negative
     */
    constexpr bool isNegative() const { return value < 0; }

    /**
     * Set the integer part of the number
     * @param v the integer part
     */
    void setInt(int v) { value = v << 4; }

    /**
     * Set the raw number
     * @param r a raw integer that will be copied verbaim to the number
     */
    void setRaw(int r) { value = r; }

    /**
     * Get te integer part of the number
     * @return the integer part
     */
    constexpr int8_t getInt() const { return value >> 4; }

    /**
     * Get the fraction part of the number; only values [0-15] are possible
     * @return [description]
     */
    constexpr int8_t getFrac() const { return (((value >= 0 ? value : ~value) & 0xF) * 10 + 7) >> 4; }

    PrintableFixedPoint12dot4 print() const {
        return PrintableFixedPoint12dot4(*this);
    }
};

#endif //FIXED_POINT_12_DOT_4_H
